from flask import Flask, render_template, request, redirect, url_for, session, flash
from models import Jogo, Usuario

app = Flask(__name__)
app.secret_key = __name__

jogos = [
    Jogo('Counter-Strike Global Offensive', 'Ação', 'PC'),
    Jogo('Call of Duty Black Ops II', 'Ação', 'PC'),
    Jogo('RESIDENT EVIL 7 biohazard  BIOHAZARD 7 resident evil', 'Terror', 'PC'),
    Jogo('Call of Duty Black Ops II - Multiplayer', 'Ação', 'PC'),
    Jogo('Left 4 Dead', 'Ação', 'PC'),
    Jogo('Left 4 Dead 2', 'Ação', 'PC')
]
usuario1 = Usuario('filipe', 'Filipe Bezerra', '@FBez')
usuario2 = Usuario('sophia', 'Sophia Bezerra', '@SBez')
usuario3 = Usuario('izabel', 'Izabel Bezerra', 'vsla')
usuario4 = Usuario('claudiomar', 'Claudiomar', 'claudio')
usuarios = {
    usuario1.id: usuario1,
    usuario2.id: usuario2,
    usuario3.id: usuario3,
    usuario4.id: usuario4
}


@app.route('/')
def inicio():
    return render_template('jogos.html', titulo='Jogos', jogos=jogos, nome_usuario=_get_nome_usuario_logado())


def _get_nome_usuario_logado():
    return usuarios[session['usuario_logado']].nome if 'usuario_logado' in session else None


@app.route('/novo')
def novo():
    if 'usuario_logado' in session:
        return render_template('novo_jogo.html', titulo='Novo Jogo', nome_usuario=_get_nome_usuario_logado())
    flash('Você deve autenticar primeiro!', category='info')
    return redirect(f'{url_for("login")}?proxima={url_for("novo")}')


@app.route('/criar', methods=['POST'])
def criar():
    nome = request.form['nome']
    categoria = request.form['categoria']
    console = request.form['console']
    novo_jogo = Jogo(nome, categoria, console)
    jogos.append(novo_jogo)
    return redirect(url_for('inicio'))


@app.route('/login')
def login():
    proxima = request.args['proxima']
    return render_template('login.html', proxima=proxima)


@app.route('/autenticar', methods=['POST'])
def autenticar():
    if request.form['usuario'] in usuarios:
        usuario_autenticando = usuarios[request.form['usuario']]
        senha = request.form['senha']
        if usuario_autenticando.senha == senha:
            session['usuario_logado'] = usuario_autenticando.id
            flash(f'Bem-vindo de volta {usuario_autenticando.nome}!', category='success')
            return redirect(request.form['proxima'] if 'proxima' in request.form else url_for('inicio'))
    flash('Usuário não encontrado', category='danger')
    if 'proxima' in request.form:
        return redirect(f'{url_for("login")}?proxima={url_for("novo")}')
    else:
        return redirect(url_for("login"))


@app.route('/logout')
def logout():
    if 'usuario_logado' in session:
        usuario_logado = session['usuario_logado']
        session.pop('usuario_logado')
        flash(f'{usuario_logado} nos vemos em breve!', category='success')
    else:
        flash('Nenhum usuário logado!', category='info')
    return redirect(url_for('inicio'))


if __name__ == '__main__':
    app.run(debug=True)
